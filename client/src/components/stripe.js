import React from "react";
import Payment from "../containers/Payment";
import { StripeProvider, Elements } from "react-stripe-elements";

const Stripe = props => {
  console.log(process.env.REACT_APP_STRIPE_PUBLIC_KEY)
  return (
    <StripeProvider apiKey='pk_test_51JZ8iWFs6kI0GyIi2MmyC3PkVAcYBUCEyz6EMRy2WV1G8WjBSXlR3wjq1T4zcFvcecRaXHONAjiTdV9xmVXTiVUn00tlIIkCcg'>
      <Elements>
        <Payment {...props} />
      </Elements>
    </StripeProvider>
  );
};

export default Stripe;
