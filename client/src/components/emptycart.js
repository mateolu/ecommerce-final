import React, { Component } from 'react';


class EmptyCart extends Component {
    render () {
        return (
            
            <div className = 'emptyCart'>
                    <h1>Your Cart is currently empty!</h1>
            </div>
            
        )
    }
}


export default EmptyCart;
