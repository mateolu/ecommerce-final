// import React, { Component } from 'react';
// import './../styles/index.css';


// class NavBar extends Component {
//     render () {
//     return (
//       <div id='NavBar'>
//         <header>

//         <ul onClick={(e)=>this.props.changePage(e.target.textContent)}>
//           <li>Home</li>
//           <li>About</li>
//           <li>Cart</li>
//           <li>Prodcuts</li>
//         </ul>
//         </header>
        
//       </div>
//     )
// }
// }

// export default NavBar;

// Temporary, to be further developed


import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './../styles/index.css';


class NavBar extends Component {
  render() {
    // const { user, logout, isLoggedin } = this.props;
    return (
      <nav className="navbar">
        <Link to={'/'}>
          <h4  className ="navLink">Home</h4>
        </Link>
        <Link to={'/about'} >
          <h4 className ="navLink">About</h4>
        </Link>
        <Link to={'/categories'} >
          <h4 className ="navLink">Categories</h4>
        </Link>
        <Link to={'/products'}>
          <h4  className ="navLink">Products</h4>
        </Link>
        <Link to={'/cart'}>
          <h4  className ="navLink">Cart</h4>
        </Link>

        {/* {this.props.isLoggedIn ? (
          <>
            <p>username: {this.props.user && this.props.user.username}</p>
            <button onClick={this.props.logout}>Logout</button>
          </>
        ) : ( */}
          <>
            <Link to="/usersarea">
              <h4 className = "navLink">Users Area</h4>
              {/* <button className="navbar-button">Users Area</button>{' '} */}
            </Link>
            <br />
            {/* <Link to="/signup">
              <button className="navbar-button">Sign Up</button>{' '}
            </Link> */}
          </>
        {/* ) */}
        
      </nav>
    );
  }
}

export default NavBar;