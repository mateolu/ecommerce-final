import React, { Component, useState, useEffect } from 'react';
import axios from 'axios';
import './../styles/productStyles.css';
import NavBar from './../components/navbar';
import Footer from './../components/footer';
import Header from './../components/header';
import {   addProductToCart, handleClick }from './../components/actions/prodActions';


const Product = (props) => {
    const [products, setProducts] = useState([])


  useEffect (() => {
     const getProducts = async () => {
        let url = 'http://localhost:3002/products/find_all';
        try {
          const res = await axios.get(url)
          setProducts(res.data);
        } catch (err) {
          console.info(err)
        }
      };
      getProducts();
      }, [])



     const handleClick = (product, price) => {
          let tempProds = {title:product, amount: 1, price:price}
          props.addToCart(tempProds)
          // console.log()
      // if/there, check if already there.
        // if (this.state.productsInCart) {
        //     { }
        // }
        // else {
        // }
      };
        return (
            <div>
             <Header/>
             <NavBar/>
<h1>Products</h1>
<div className='prodDisplay'>
            <h3>All products</h3>
            {/* {this.state.name.lastName && this.state.name.lastName.name} */}
        {products && products.map((prod) => {
            return (<div className='prodGrid'>
                <div className='product'>                {prod.title}  
                <button
                onClick={() => {
                  handleClick(prod.title, prod.price);
                }}> ${prod.price} Add to cart </button>
 </div>
                </div>)
        })}
            </div>
<Footer/>
            </div>
        )
}
export default Product;






