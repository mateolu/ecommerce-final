import React, { Component } from 'react';
import axios from 'axios';
import NavBar from './../components/navbar';
import './../styles/index.css';
import Footer from './../components/footer';
import Header from './../components/header';


class Categories extends Component {

    state = {
        categories: [],
        name: ""
    }
    componentDidMount(){
        this.getCategories()
    }
    getCategories = () => {
      let url = 'http://localhost:3002/categories/find_all';
      axios.get(url)
      .then((res)=>{
        //  let { Title, Year } = res.data;
         this.setState({categories: res.data })
        console.log(res)
      })
      .catch((error)=>{
        debugger
        this.setState({error:'something went wrong'})
      })
    }


    render () {
        console.log(this.state.categories)
        return (
            <div>
            <div>
                <Header/>
                <NavBar/>
            </div>
            <div className='catDisplay'>
            <h1>Categories!!!</h1>
            {/* {this.state.name.lastName && this.state.name.lastName.name} */}
        {this.state.categories && this.state.categories.map((cat) => {
            return (<div className='catGrid'>
                {cat.title}
                </div>)
        })}

            </div>
            
                <Footer/>
            </div>
        )
    }
}

export default Categories;
