// import React, { Component, useEffect, useState } from 'react';
// import axios from 'axios';
// import NavBar from './../components/navbar';
// import './../styles/index.css';
// import Footer from './../components/footer';
// import Header from './../components/header';


// const Payment = (props) => {

 


//     const [totalCheck, setTotalCheck ] = useState([])


//     useEffect (() => {
//         const getPrice = async () => {
//            let url = 'http://localhost:3002/create-checkout-session"';
//            try {
//              const res = await axios.get(url)
//              setTotalCheck(res.data);
//            } catch (err) {
//              console.info(err)
//            }
//          };
//          getPrice();
//          }, [])
    
//         console.log(totalCheck)
//         console.log(props)
//         return (
//             <div>
//             <div>
//                 <Header/>
//                 <NavBar/>
//             </div>
//             <div className='catDisplay'>
//             <h1>Your checkout</h1>
//         {/* {totalCheck && totalCheck.map((cart) => {
//             return (<div className='catGrid'>
//                 {props.total}
//                 </div>)
//         })} */}
// {props.total}
//             </div>
            
//                 <Footer/>
//             </div>
//         )
    
// }

// export default Payment;

// import React, { Component, useEffect, useState } from 'react';
// import axios from 'axios';
// import NavBar from './../components/navbar';
// import './../styles/index.css';
// import Footer from './../components/footer';
// import Header from './../components/header';

import React, { Component, useEffect, useState } from 'react';
// import Product from "../components/product";
import NavBar from './../components/navbar';
import './../styles/index.css';
import Footer from './../components/footer';
import Header from './../components/header';
import axios from "axios";
import { injectStripe } from "react-stripe-elements";

const Payment = props => {
  //=====================================================================================
  //=======================  CALCULATE TOTAL FUNCTION  ==================================
  //=====================================================================================
//   const calculate_total = () => {
//     let total = 0;
//     products.forEach(ele => (total += ele.quantity * ele.amount));
//     return total;
//   };
  //=====================================================================================
  //=======================  CREATE CHECKOUT SESSION  ===================================
  //=====================================================================================
  const createCheckoutSession = async () => {
    try {
      const response = await axios.post(

        "http://localhost:3002/checkout-session",
        { props }
      );
      
      return response.data.ok
        ? (localStorage.setItem(
            "sessionId",
            JSON.stringify(response.data.sessionId)
          ),
          redirect(response.data.sessionId))
        : props.history.push("/payment/error");
    } catch (error) {
      props.history.push("/payment/error");
    }
  };
  //=====================================================================================
  //=======================  REDIRECT TO STRIPE CHECKOUT  ===============================
  //=====================================================================================
  const redirect = sessionId => {
    debugger;
    props.stripe
      .redirectToCheckout({
        // Make the id field from the Checkout Session creation API response
        // available to this file, so you can provide it as parameter here
        // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
        sessionId: sessionId
      })
      .then(function(result) {
        debugger;
        // If `redirectToCheckout` fails due to a browser or network
        // error, display the localized error message to your customer
        // using `result.error.message`.
      });
  };
  //=====================================================================================
  //=====================================================================================
  //=====================================================================================
  return (
    <div className="checkout_container">
            <Header/>
             <NavBar/>
      <div className="header">Checkout - Check products before pay</div>
      {/* <div className="products_list">
        {products.map((item, idx) => {
          return <Product key={idx} {...item} />;
        })}
      </div> */}
      <div className="footer">
        <div className="total">Total : {props.total} €</div>
        <button className="button" onClick={() => createCheckoutSession()}>
          PAY
        </button>
      </div>
      <Footer/>
    </div>
  );
};

export default injectStripe(Payment);
// export default Payment(Checkout)

