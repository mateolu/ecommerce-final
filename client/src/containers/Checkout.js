import React, { useEffect } from "react";
import axios from "axios";

const PaymentSuccess = props => {
  useEffect(() => {
    getSessionData();
  }, []);
  const getSessionData = async () => {
    try {
      const sessionId = JSON.parse(localStorage.getItem("sessionId"));
      const response = await axios.get(
        `http://localhost:4242/payment/checkout-session?sessionId=${sessionId}`
      );
      localStorage.removeItem("sessionId");
      console.log("== response ==>", response);
      //if you need the products list in this page, you can find them in : response.data.session.display_items
    } catch (error) {
      //handle the error here, in case of network error
      debugger;
    }
  };
  return (
    <div className="message_container">
      <div style={{ border: "2px solid  #35BFDE" }} className="message_box">
        <div className="message_box_left">
          <img
            alt="smile_icon"
            className="image"
            src={
              "https://res.cloudinary.com/estefanodi2009/image/upload/v1578495645/images/smile.png"
            }
          />
        </div>
        <div style={{ color: "#35BFDE" }} className="message_box_right">
          Payment Successfull
        </div>
      </div>
    </div>
  );
};

export default PaymentSuccess;
