import React, { Component } from 'react';
import Products from './Products';
import NavBar from './../components/navbar'
import Categories from './Categories';
import './../styles/index.css';
import Footer from './../components/footer';
import Header from './../components/header';


class Default extends Component {
    render () {
        return (
            <div className='Homepage'>
                <Header/>
                <NavBar/>
                <div className ='Home2'>
                    <h1>Welcome to our lovely shop!</h1>
                    <h3>Here is a short preview of our products</h3>
                <Footer/>
</div>


            </div>
        )
    }
}

export default Default;
