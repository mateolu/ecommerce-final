import React, { Component, useState, useEffect } from 'react';
import { Switch, Route,  BrowserRouter as Router} from 'react-router-dom';
import Default from './containers/Default';
import NavBar from './components/navbar';
import About from './containers/about';
import Products from './containers/Products'
import Cart from './components/cart';
import Categories from './containers/Categories';
import UsersArea from './containers/UsersArea';
import loginContainer from './containers/loginContainer';
import Register from './components/loginall/register';
import SecretPage from './components/loginall/secretpage';
import Payment from './containers/Payment';
import injectStripe from './containers/Payment';
import Stripe from './components/stripe'

import './styles/index.css';

const App = () => {
  const [cart, setCart] = useState([])
  const [productsInCart,setProductsInCart ] = useState([])
  const [totalPrice, setTotalPrice] = useState([])
  useEffect(()=> {
    console.log('not supposed to be here.')
    localStorage.setItem( 'cart', {cart})
    
   
  }, [cart])



  useEffect (() => {
    console.log('here-----------------------------------------------')
    console.log(cart)
    let cartSum = 0;
    console.log(cartSum)
    cart.forEach ((e) => {
      cartSum += e.price
    })
    console.log(cartSum)

    setTotalPrice(cartSum)
  })

  let addToCart = (item) => setCart((currentCart) => [...currentCart, item]);

  let clearCart = () => {
    setCart ([])
  }

  
  let amountOfItems = (id) => cart.filter((item) => item.id === id).length;
  // let listItemsToBuy = () => cart.map((item) => (
  //   <div key={item.id}>
  //     {`${item.title}: $${item.price}`}
  //     <button type="submit" onClick={() => addToCart(item)}>Add</button>
  //   </div>
  // ));
  const listItemsInCart = () => cart.map((item) => (
    <div key={item.id}>
      ({amountOfItems(item.id)} x ${item.price}) {`${item.title}`}
      <button type="submit" onClick={() => removeFromCart(item)}>Remove</button>
    </div>
  ));


  // might need some work done here
  const removeFromCart = (item) => {
    setCart((currentCart) => {
      const indexOfItemToRemove = currentCart.findIndex(
        (cartItem) => cartItem.id === item.id
        );
      if (indexOfItemToRemove === -1) {
        return currentCart;
      }
      return [
        ...currentCart.slice(0, indexOfItemToRemove),
        ...currentCart.slice(indexOfItemToRemove + 1),
      ];
    });
  };
   return (
     <React.Fragment>
       <Router>
       <Switch>
         <Route path="/default" component = {Default}/>
         <Route path="/register" component = {Register}/>
         <Route 
         path="/create-checkout-session" 
         render={(props)=> (
            <Stripe 
            {...props} 
            total = {totalPrice}
            />

         )}
         />
         <Route path="/loginhere" component = {loginContainer}/>
         <Route
          path="/cart"
          render={(props) => (
            <Cart
              {...props}
              cart={Cart}
              removeFromCart={removeFromCart}
              addToCart={addToCart}
              products = {cart}
              total = {totalPrice}
              clear = {clearCart}
              amount = {amountOfItems}
              list = {listItemsInCart}
              />
          )}
         />
         <Route path='/categories'><Categories></Categories></Route>
         <Route
          path='/products'
          render={(props) => <Products {...props}  addToCart={addToCart} productsToCart={setProductsInCart}/>}
         />
         <Route path='/usersarea' component = {UsersArea}/>
         <Route path='/login'  component = {loginContainer}/>
         <Route path="/navbar" component={NavBar}/>
         <Route path='/about' component={About}/>
         <Route component={Default}/>
       </Switch>
       </Router>
     </React.Fragment>
    );
} 

export default App;