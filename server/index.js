const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const config = require('config');
const categories = require('./routes/categoriesRoute')
const products = require('./routes/productsRoute')
const users = require('./routes/usersRoute')

const cors = require('cors');
const bodyParser = require('body-parser')


const app = express();
// app.use(bodyParser.urlencoded({extended: true}))
// app.use(bodyParser.json())
app.use(express.json());
app.use(cors({
    origin: 'http://localhost:3000'
}));


app.use('/categories', categories)
app.use('/products', products)
app.use('/users', users)
const port = process.env.PORT || 3002;

// app.listen(port, () => console.log('listening on port ', port))




// const dbURI = config.get('dbURI');
mongoose.connect('mongodb+srv://dbUser:dbUser1@cluster0.3ken9.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex:true })
  .then((result) => app.listen(port))
  .catch((err) => console.log(err));


  // async function connecting(){
  //   try {
  //     await mongoose.connect('mongodb+srv://dbUser:dbUser1@cluster0.3ken9.mongodb.net/test?retryWrites=true&w=majority', { useNewUrlParser: true, useUnifiedTopology: true })
  //     console.log ('connected to db')
  //   } catch {error} {
  //     console.log('Error, db not running')
  //   }
  // }
  // connecting()
  // mongoose.set('useCreateIndex', true)

  // await mongoose.connect('mongodb+srv://dbUser:dbUser1@cluster0.3ken9.mongodb.net/myFirstDatabase?retryWrites=true&w=majority')
 