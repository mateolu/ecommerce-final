const router = require("express").Router();
const controller = require("../controllers/payment.controller.js");

// POST  ==>  '/payment/create_checkout_session'
// GET   ==>  '/payment/checkout_session'


router.post("/create-checkout-session", controller.create_checkout_session);
router.get("/checkout-session", controller.checkout_session);

module.exports = router;
