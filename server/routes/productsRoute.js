const express     = require('express'), 
    router        = express.Router(),
    productsController    = require('../controllers/productsController');


// POST  ==>  '/products/create'
// POST  ==>  '/products/delete'
// POST  ==>  '/products/update'
// GET   ==>  '/products/find_all'
// GET   ==>  '/products/find_one/:product_id'

router.post('/create',productsController.create);
router.post('/delete',productsController.delete);
router.post('/update',productsController.update);
router.get('/find_all', productsController.findAll);
router.get('/find_one/:product_id', productsController.findOne);

module.exports = router;


