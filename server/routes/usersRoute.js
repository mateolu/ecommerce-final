const express     = require('express'), 
    router        = express.Router(),
    usersController    = require('../controllers/usersController');


// POST  ==>  '/users/register'
// POST  ==>  '/users/login'
// GET   ==>  '/users/verify_token'


router.post('/users/register',usersController.register);
router.post('/users/login',usersController.login);
router.get('/users/verify_token', usersController.verify_token);

module.exports = router;

