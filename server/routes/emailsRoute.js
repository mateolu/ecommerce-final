const express     = require('express'), 
    router        = express.Router(),
    emailsController    = require('../controllers/emailsController');


    // POST  ==>  '/emails/send_contact_email'


router.post('/emails/send_contact_email',emailsController.send_email);

module.exports = router;