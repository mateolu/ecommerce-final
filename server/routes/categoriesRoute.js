const express     = require('express'), 
    router        = express.Router(),
    categoriesController    = require('../controllers/categoriesController');


    // POST  ==>  '/categories/create'
    // POST  ==>  '/categories/delete'
    // POST  ==>  '/categories/update'
    // GET   ==>  '/categories/find_all'
    

router.post('/create', categoriesController.create);
router.post('/delete',categoriesController.delete);
router.post('/update',categoriesController.update);
router.get('/find_all', categoriesController.findAll);

module.exports = router;