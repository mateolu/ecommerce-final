    // POST  ==>  '/categories/create'
    // POST  ==>  '/categories/delete'
    // POST  ==>  '/categories/update'
    // GET   ==>  '/categories/find_all'

// router.post('/categories/create',categoriesController.create);
// router.post('/categories/delete',categoriesController.delete);
// router.post('/categories/update',categoriesController.update);
// router.get('/categories/find_all', categoriesController.findAll);


const Categories = require('../models/categoriesModel');

class categoriesController {
     // CREATE CATEGORY
     async create (req, res) {
        console.log('creating', req.body)
        let { title, description } = req.body;
        try{
            const added = await Categories.create({title, description});
            res.send(added)
        }
        catch(e){
            res.send({e})
        }
    }
 
     // DELETE CATEGORY
    async delete (req, res){
      console.log('delete!!!')
      let { category } = req.body;
      try{
          const removed = await Categories.deleteOne({ category });
          res.send({removed});
         }
         catch(error){
             res.send({error});
         };
    }


         // UPDATE Category

    async update (req, res){
        let { category, newCategory } = req.body.category;
        try{
            const updated = await Categories.updateOne(
                { category },{ category:newCategory}
             );
            res.send({updated});
        }
        catch(error){
            res.send({error});
        };
    }


    // GET FIND ALL
    async findAll(req, res){
        try{
            const categories = await Categories.find({});
            res.send(categories);
        }
        catch(e){
            res.send({e})
        }
    }


};
module.exports = new categoriesController();