// router.post('/products/create',productsController.create);
// router.post('/products/delete',productsController.delete);
// router.post('/products/update',productsController.update);
// router.get('/products/find_all', productsController.findAll);
// router.get('/products/find_one/:product_id', productsController.findOne);

const Products = require('../models/productsModel');

class productsController {
    // CREATE PRODUCT
    async create (req, res) {
        console.log('creating', req.body)
        let { title, description, category, price } = req.body;
        try{
            const added = await Products.create({title, description, category, price});
            res.send(added)
        }
        catch(e){
            res.send({e})
        }
    }

    // async create (req, res) {
    //     console.log('creating', req.body)
    //     let { title, description } = req.body;
    //     try{
    //         const added = await Categories.create({title, description});
    //         res.send(added)
    //     }
    //     catch(e){
    //         res.send({e})
    //     }
    // }



    // DELETE PRODUCT
    async delete (req, res){
        console.log('delete!!!')
        let { product } = req.body;
        try{
            const removed = await Products.deleteOne({ product });
            res.send({removed});
        }
        catch(error){
            res.send({error});
        };
    }

    // UPDATE TODO

    async update (req, res){
        let { product, newProduct } = req.body;
        try{
            const updated = await Products.updateOne(
                { product },{ product:newProduct}
             );
            res.send({updated});
        }
        catch(error){
            res.send({error});
        };
    }

    // GET FIND ALL
    async findAll(req, res){
        try{
            const products = await Products.find({});
            res.send(products);
        }
        catch(e){
            res.send({e})
        }
    }

    // FIND ONE PRODUCT BY _ID
    async findOne(req ,res){
        let { product_id} = req.params;
        try{
            const product = await Products.findOne({_id:product_id});
            res.send(product);
        }
        catch(e){
            res.send({e})
        }

    }



};

module.exports = new productsController();


